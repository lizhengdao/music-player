# Music Player Example

This is a sample project for the Flutter Music Player library. 

The implementation can be found in [main.dart](lib/main.dart).

Prerequisites and installation instructions for the plugin can be found in the [project readme](../README.md). 

![Example Notification](notification.png)
